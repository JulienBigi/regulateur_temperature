
#include <Wire.h>
#include <Adafruit_RGBLCDShield.h>
#include <utility/Adafruit_MCP23017.h>
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();
#include <Adafruit_MAX31865.h>
/* Les PIN SPI de la carte Arduino MEGA2560 sont: 
                                          CS, DI, DO, CLK  */
Adafruit_MAX31865 max = Adafruit_MAX31865(53, 51, 50, 52);
// Utilisation que d'un seul PIN SPI, le PIN CS
// Adafruit_MAX31865 max = Adafruit_MAX31865(53);

#define RREF 430.0 // Valeur de la RREF du module MAX31865
#define WHITE 0x7 // Couleur de l'afficheur LCD
#define SAMPLE 20 // Nombre d'échantillion pour faire la moyenne glissante, afin de lisser la mesure affiché sur l'afficheur LCD.

/*************************    PIN DES I/O    *************************/
#define BUTTON_GAUCHE 6
#define BUTTON_DROIT 5
#define RELAIS 7
#define ON_OFF 4

#define tempMax 300

float TempSum = 0, valTemperature[20], TemperatureMoyenne, mesure; // Tableau et variable pour la moyenne glissante (lissage de la mesure)
int t = 0;
/*                              Variables du correcteur PI                             */
/*                                                                                      */
double Kp = 3, Ki = 300, erreur, valeurInt, valeurP, OP, outMin, outMax, intervalSec, tCond; 
int etatBouton1, etatBouton3, etatBouton, etatRelais, lastButtonState, dernierEtatRelais, compteur = 0;

unsigned int consigne = 85, BP_PLUS, BP_MOINS, MEM_BP_PLUS, MEM_BP_MOINS; // Consigne de départ basé sur la température ambiante moyenne ~25°C
unsigned long previousMillis = millis(), interval, tCy = 4000, temps_ecoule = 0; // Base de temps du programme
unsigned long temps_actuel, temps_appui, mem_first_millis, mem_next_millis;

void setup() {
  pinMode(BUTTON_GAUCHE, INPUT_PULLUP);
  pinMode(BUTTON_DROIT, INPUT_PULLUP);
  pinMode(ON_OFF, INPUT_PULLUP);
  max.begin(MAX31865_2WIRE);
  pinMode(RELAIS, OUTPUT);
  Serial.begin(9600);
  lcd.begin(16, 2);
}
void loop() {
  interval = millis() - previousMillis;
  intervalSec = ((double)interval / 1000);
  previousMillis = millis();
  
  etatRelais = digitalRead(RELAIS); // digitalRead() premet de lire l'état d'une entrée ou d'une sortie digital. Donc etatRelais équivaux à lire l'état actuel du relais.
  
  moyenne();
  setMode();
  pilotageSortie();
  /*Serial.println(interval);*/Serial.print(" , ");Serial.print(etatRelais);Serial.print("\n");
  lcd.setCursor(0, 0);lcd.print("Temp: ");
  lcd.setCursor(0, 1);lcd.print("Cons:");lcd.setCursor(7, 1); lcd.print(consigne);lcd.setCursor(13,1);lcd.print((char)223);lcd.setCursor(14,1);lcd.print("C"); 
  reglageConsigne();
}

/*****************************  Fonction du réglage de la consigne  *****************************/
void reglageConsigne() {
  BP_PLUS = digitalRead(BUTTON_DROIT);
  BP_MOINS = digitalRead(BUTTON_GAUCHE);

if ( BP_PLUS == LOW ) {
    
    temps_actuel = millis();

    if ( MEM_BP_PLUS == HIGH ) {
        mem_first_millis = temps_actuel;
        mem_next_millis  = temps_actuel;
    }  
    if ( temps_actuel >= mem_next_millis ) {
        
        temps_appui = ( temps_actuel - mem_first_millis );
       
        if ( temps_appui < 5000 ) { // Si l'appui sur le BP_PLUS est supérieur à 5sec alors l'incrémentation de ma consigne augemente de 5 en 5.
            mem_next_millis = ( temps_actuel + 500 );
        } else {
            mem_next_millis = ( temps_actuel + 500 );
            consigne+=4;
        }
        consigne+=1; if(consigne == 300) consigne = 200; // Limte de consigne, si la consigne dépasse 300°. La consigne redescend à 200°.
        lcd.setCursor(7, 1); lcd.print(consigne);lcd.print(" ");//lcd.print((char)223); lcd.print(" ");    
    } 
}
        MEM_BP_PLUS = BP_PLUS;
if ( BP_MOINS == LOW ) {
    
    temps_actuel = millis();
    
    if ( MEM_BP_MOINS == HIGH ) {
        mem_first_millis = temps_actuel;
        mem_next_millis  = temps_actuel;
    }
    if ( temps_actuel >= mem_next_millis ) {
        
        temps_appui = ( temps_actuel - mem_first_millis );
        
        if ( temps_appui < 5000 ) { // Si l'appui sur le BP_MOINS est supérieur à 5sec alors l'incrémentation de ma consigne diminue de 5 en 5.
            mem_next_millis = ( temps_actuel + 500 );
        } else {
            mem_next_millis = ( temps_actuel + 500 );
            consigne-=4; 
        }
        consigne-=1; if(consigne == 0) consigne = 20; // Limite de consigne, si la consigne déscend en dessous de 0°. La consinge revient à 20°.
        lcd.setCursor(7, 1); lcd.print(consigne);lcd.print(" ");//lcd.print((char)223); lcd.print(" ");           
    } 
}
        MEM_BP_MOINS = BP_MOINS;  
}
/*****************************  Fonction pour lisser la mesure  *****************************/
void moyenne() {
  TempSum = TempSum - valTemperature[t]; 
  valTemperature[t] = max.temperature(100, RREF);
  TempSum += valTemperature[t];
  TemperatureMoyenne = TempSum / SAMPLE;
  lcd.setCursor(7, 0);lcd.print(TemperatureMoyenne);
  lcd.setCursor(13, 0);lcd.print((char)223); lcd.print("C");  
  t++;
  if (t > (SAMPLE - 1)) {
    t = 0;
  }
}
/*****************************  Fonction mise en veille du systéme  *****************************/
void setMode() { // Mise en veille/allumage de l'afficheur LCD + choix du mode de regulation AUTO (correcteur PI en fonctionnement) ou MANU (arrêt du correcteur PI, sytème OFF).
  etatBouton = digitalRead(ON_OFF);

  if (etatBouton != lastButtonState) {
    if (etatBouton == HIGH) {
      compteur++;
    }else{
         }
    lastButtonState = etatBouton;
    }
  if (compteur % 2 == 0) {
    lcd.display();
    modeAuto();
    lcd.setBacklight(WHITE);
  }else{
    lcd.setBacklight(false);
    modeManu();
    lcd.setCursor(7, 1);
    lcd.clear();
    lcd.noDisplay();
  }
}
/*****************************  Fonction du mode MANU  *****************************/
void modeManu() { // Le mode MANUEL, sert à arrêter le calcul du correcteur PI (la valeur de P et de I mis à 0) et met la sortie du regulateur (OP) à 0.
  valeurInt = 0;
  valeurP = 0;
  OP = 0;    
  Limites(0, 100);
  consigne = 85;
}

/*****************************  Fonction du mode AUTO  *****************************/
void modeAuto() {

  mesure = max.temperature(100, RREF); // Acquisition de la T° grâce au MODULE MAX31865 (convertisseur PT100 pour Arduino) cf.(Exemples/Adafruit_MAX31865 Library/max 31865).

  erreur = ((consigne - mesure) * 100) / tempMax; // Direction de mon regulateur soit DIRECT, soit INVERSE. Ici notre regulateur est INVERSE d'où l'erreur est egale à ma consigne - la mesure + conversion en pourcentage de l'erreur.

  valeurP  = ( Kp * erreur );                          /*                                                                */
  valeurInt += ((intervalSec / Ki) * erreur ) * Kp;   /*                      CORRECTEUR PI                             */
  OP = valeurP + valeurInt;                          /*                                                                */
    
  Limites(0, 100); // Limites de sortie de mon régulateur
 
  Serial.print(consigne);Serial.print(" , ");Serial.print(mesure);Serial.print(" , ");Serial.print(OP);Serial.print(" , ");Serial.print(valeurP);Serial.print(" , ");Serial.print(valeurInt);
  
  
}

/*****************************  Fonction de  pilotage du relais  *****************************/
void pilotageSortie(){ 
  tCond = ((double)tCy/100) * OP; // Calcul du temps de conduction, afin de pouvoir agir sur le relais.
  temps_ecoule += interval; // Le temps écoulé depuis le début du programme.
  //Serial.print(" , ");Serial.print(temps_ecoule);
   if(temps_ecoule >= tCy){
      temps_ecoule=0;
   }
   if(tCond > temps_ecoule){
    digitalWrite(RELAIS, HIGH);
   }else{
    digitalWrite(RELAIS, LOW);
   }
}

/*****************************  Fonction des limites de la sortie (0/100%)  *****************************/
void Limites(double Min, double Max) {
  outMin = Min;
  outMax = Max;

  if (OP > outMax) OP = outMax;
  else if (OP < outMin) OP = outMin;

  if (OP >= outMax) valeurInt = OP - valeurP;
  else if (OP <= outMin) valeurInt = OP - valeurP;
}
